## !!!! Warning: order matters !!!!
################# Tweaks ##################
_HZ=500
scripts/config -d HZ_300 -e "HZ_${_HZ}" --set-val HZ "${_HZ}"
echo "Setting tick rate to ${_HZ} Hz (1000 Hz is for super interactivity, 125 Hz is for servers)..."

scripts/config -d CPU_MITIGATIONS
echo "Disabling CPU mitigations (free performance on older CPUs)..."

scripts/config -e TRANSPARENT_HUGEPAGE_MADVISE
echo "Enabling madvise THPs..."

# BBR - better handling in general
# Westwood - low quality link king, smartphones
# HTCP - good for high speed practically lossless lan
scripts/config -e TCP_CONG_HTCP \
               -e TCP_CONG_WESTWOOD \
               -e TCP_CONG_BBR
scripts/config -e DEFAULT_BBR --set-str DEFAULT_TCP_CONG bbr
echo "Setting default TCP congestion to BBR..."

scripts/config -e BPFILTER
echo "Enabling eBPF for Netfilter..."

# FQ - a requirement of BBR
# CAKE - good for QoS in routers
scripts/config -e NET_SCH_CAKE \
               -e NET_SCH_FQ
echo "Enabling extra queue scheduling algorithms..."

scripts/config --set-val SND_HDA_POWER_SAVE_DEFAULT 0
echo "Disable Intel HD Audio powersave..."

# CachyOS tweaks
scripts/config -e RCU_NOCB_CPU_DEFAULT_ALL
echo "Offloading RCU from all CPUs..."

################# Debloat ##################
scripts/config -e EXPERT
echo "Enabling expert mode for further tweaks..."

scripts/config -e PROCESSOR_SELECT \
               -d CPU_SUP_HYGON \
               -d CPU_SUP_AMD \
               -d CPU_SUP_CENTAUR \
               -d CPU_SUP_ZHAOXIN \
               -e CPU_SUP_INTEL
echo "Configuring CPU support (only Intel)..."

scripts/config -d HAMRADIO \
               -d CAN \
               -d NFC \
               -d NET_9P \
               -d ATM_DRIVERS \
               -d NET_DSA \
               -d ATALK \
               -d PHONET \
               -d BATMAN_ADV \
               -d BCMA
echo "Removing useless networking devices and protocols..."

scripts/config -d FPGA \
               -d SOUNDWIRE \
               -d MELLANOX_PLATFORM \
               -d CHROME_PLATFORMS \
               -d ACCESSIBILITY \
               -d DRM_ACCEL_QAIC \
               -d DRM_ACCEL_HABANALABS \
               -d GNSS \
               -d MACINTOSH_DRIVERS \
               -d AMD_SFH_HID \
               -d IIO \
               -d SOC_TI \
               -d WPCM450_SOC \
               -d GOOGLE_FIRMWARE \
               -d MMC \
               -d SCSI_UFSHCD \
               -d MEMSTICK \
               -d SIOX \
               -d SLIMBUS
echo "Removing extra device drivers..."

scripts/config -d ASUS_LAPTOP \
               -d ACERHDF \
               -d ACER_WIRELESS \
               -d ACER_WMI \
               -d ASUS_WMI \
               -d ASUS_TF103C_DOCK \
               -d MERAKI_MX100 \
               -d X86_PLATFORM_DRIVERS_DELL \
               -d AMILO_RFKILL \
               -d FUJITSU_LAPTOP \
               -d FUJITSU_TABLET \
               -d X86_PLATFORM_DRIVERS_HP \
               -d MSI_EC \
               -d MSI_LAPTOP \
               -d MSI_WMI \
               -d MLX_PLATFORM \
               -d SYSTEM76_ACPI \
               -d PANASONIC_LAPTOP \
               -d SONY_LAPTOP \
               -d SAMSUNG_LAPTOP \
               -d GPD_POCKET_FAN
echo "Removing unused manufacturer specific drivers (except Lenovo)..."

scripts/config -d WLAN_VENDOR_ADMTEK \
               -d WLAN_VENDOR_ATH \
               -d WLAN_VENDOR_ATMEL \
               -d WLAN_VENDOR_BROADCOM \
               -d WLAN_VENDOR_CISCO \
               -d WLAN_VENDOR_INTERSIL \
               -d IPW2200 \
               -d IWL3945 \
               -d IWL4965 \
               -d WLAN_VENDOR_MARVELL \
               -d WLAN_VENDOR_MEDIATEK \
               -d WLAN_VENDOR_MICROCHIP \
               -d WLAN_VENDOR_PURELIFI \
               -d WLAN_VENDOR_RALINK \
               -d WLAN_VENDOR_REALTEK \
               -d WLAN_VENDOR_RSI \
               -d WLAN_VENDOR_SILABS \
               -d WLAN_VENDOR_ST \
               -d WLAN_VENDOR_TI \
               -d WLAN_VENDOR_ZYDAS \
               -d WLAN_VENDOR_QUANTENNA \
               -d PCMCIA_RAYCS \
               -d PCMCIA_WL3501
echo "Removing extra WLAN device vendors (except Intel)..."

scripts/config -d NET_VENDOR_3COM \
               -d NET_VENDOR_ADAPTEC \
               -d NET_VENDOR_AGERE \
               -d NET_VENDOR_ALACRITECH \
               -d NET_VENDOR_ALTEON \
               -d ALTERA_TSE \
               -d NET_VENDOR_AMAZON \
               -d NET_VENDOR_AMD \
               -d NET_VENDOR_AQUANTIA \
               -d NET_VENDOR_ARC \
               -d NET_VENDOR_ASIX \
               -d NET_VENDOR_ATHEROS \
               -d NET_VENDOR_CISCO \
               -d NET_VENDOR_CORTINA \
               -d NET_VENDOR_DAVICOM \
               -d DNET \
               -d NET_VENDOR_DEC \
               -d NET_VENDOR_DLINK \
               -d NET_VENDOR_EMULEX \
               -d NET_VENDOR_ENGLEDER \
               -d NET_VENDOR_EZCHIP \
               -d NET_VENDOR_FUJITSU \
               -d NET_VENDOR_FUNGIBLE \
               -d NET_VENDOR_GOOGLE \
               -d NET_VENDOR_HUAWEI \
               -d NET_VENDOR_INTEL \
               -d NET_VENDOR_MARVELL \
               -d NET_VENDOR_MELLANOX \
               -d NET_VENDOR_MICREL \
               -d NET_VENDOR_MICROCHIP \
               -d NET_VENDOR_MICROSEMI \
               -d NET_VENDOR_MICROSOFT \
               -d NET_VENDOR_MYRI \
               -d FEALNX \
               -d NET_VENDOR_NI \
               -d NET_VENDOR_NATSEMI \
               -d NET_VENDOR_NETERION \
               -d NET_VENDOR_NETRONOME \
               -d NET_VENDOR_NVIDIA \
               -d NET_VENDOR_OKI \
               -d ETHOC \
               -d NET_VENDOR_PACKET_ENGINES \
               -d NET_VENDOR_PENSANDO \
               -d NET_VENDOR_QLOGIC \
               -d NET_VENDOR_BROCADE \
               -d NET_VENDOR_QUALCOMM \
               -d NET_VENDOR_RDC \
               -d NET_VENDOR_RENESAS \
               -d NET_VENDOR_ROCKER \
               -d NET_VENDOR_SAMSUNG \
               -d NET_VENDOR_SEEQ \
               -d NET_VENDOR_SILAN \
               -d NET_VENDOR_SIS \
               -d NET_VENDOR_SOLARFLARE \
               -d NET_VENDOR_SMSC \
               -d NET_VENDOR_SOCIONEXT \
               -d NET_VENDOR_STMICRO \
               -d NET_VENDOR_SUN \
               -d NET_VENDOR_SYNOPSYS \
               -d NET_VENDOR_TEHUTI \
               -d NET_VENDOR_TI \
               -d NET_VENDOR_VERTEXCOM \
               -d NET_VENDOR_VIA \
               -d NET_VENDOR_WANGXUN \
               -d NET_VENDOR_WIZNET \
               -d NET_VENDOR_XILINX \
               -d NET_VENDOR_XIRCOM
echo "Removing extra Ethernet device vendors (except Realtek)..."

scripts/config -d REISERFS_FS \
               -d JFS_FS \
               -d JFFS2_FS \
               -d BEFS_FS \
               -d HFS_FS \
               -d HFSPLUS_FS \
               -d AFFS_FS \
               -d XFS_FS \
               -d GFS2_FS \
               -d OCFS2_FS \
               -d BTRFS_FS \
               -d ORANGEFS_FS \
               -d NFSD \
               -d RPCSEC_GSS_KRB5 \
               -d CEPH_FS \
               -d AFS_FS \
               -d CODA_FS \
               -d OCFS2_FS \
               -d UBIFS_FS
echo "Removing extra filesystems (leaving mostly ext4, ntfs, f2fs and some networking)..."

scripts/config -d DRM_AMDGPU \
               -d DRM_RADEON \
               -d DRM_NOUVEAU
echo "Removing really heavy bloat (AMD GPU support, nouveau)..."

scripts/config -d DRM_GMA500 \
               -d DRM_MGAG200 \
               -d AGP
echo "Removing extra graphics drivers..."
 
scripts/config -d MEDIA_ANALOG_TV_SUPPORT \
               -d MEDIA_DIGITAL_TV_SUPPORT \
               -d MEDIA_RADIO_SUPPORT \
               -d RC_CORE
echo "Removing useless media devices..."

scripts/config -d SND_SOC_XILINX_I2S \
               -d SND_SOC_XILINX_AUDIO_FORMATTER \
               -d SND_SOC_XILINX_I2S \
               -d SND_SOC_IMG \
               -d SND_I2S_HI6210_I2S \
               -d SND_DESIGNWARE_I2S \
               -d SND_BCM63XX_I2S_WHISTLER \
               -d SND_ATMEL_SOC \
               -d SND_SOC_ADI \
               -d SND_SOC_AMD_ACP \
               -d SND_SOC_AMD_ACP3x \
               -d SND_SOC_AMD_RENOIR \
               -d SND_SOC_AMD_ACP5x \
               -d SND_SOC_AMD_ACP6x \
               -d SND_SOC_AMD_ACP_COMMON \
               -d SND_SOC_AMD_RPL_ACP6x \
               -d SND_SOC_AMD_PS \
               -d SND_SOC_SOF_TOPLEVEL \
               -d SND_AMD_ACP_CONFIG \
               -d SND_PCMCIA \
               -d SND_SOC_CHV3_I2S
echo "Removing useless audio devices (except Intel)..."

scripts/config -d SND_HDA_CODEC_SI3054 \
               -d SND_HDA_CODEC_CMEDIA \
               -d SND_HDA_CODEC_CA0132 \
               -d SND_HDA_CODEC_CA0110 \
               -d SND_HDA_CODEC_CONEXANT \
               -d SND_HDA_CODEC_CS8409 \
               -d SND_HDA_CODEC_CIRRUS \
               -d SND_HDA_CODEC_VIA \
               -d SND_HDA_CODEC_SIGMATEL \
               -d SND_HDA_CODEC_ANALOG \
               -d SND_HDA_SCODEC_CS35L41_SPI \
               -d SND_HDA_SCODEC_CS35L41_I2C
echo "Removing useless audio codecs (except Realtek)..."
# TODO: more are hidden in submenus

scripts/config -d INFINIBAND \
               -d FIREWIRE
echo "Removing some server equipment support..."

scripts/config -d QLGE \
               -d FIELDBUS_DEV \
               -d PI433 \
               -d KS7010 \
               -d LTE_GDM724X \
               -d VT6656 \
               -d VT6655 \
               -d PRISM2_USB \
               -d RTL8192U \
               -d RTLLIB \
               -d RTL8723BS \
               -d R8712U \
               -d RTS5208
echo "Removing some staging drivers..."

