Base:
* Stock Arch kernel base config
* Some patches from [CachyOS](https://github.com/CachyOS/kernel-patches)
* and [linux-tkg](https://github.com/Frogging-Family/linux-tkg)

Patches:
* 500Hz default timer
* Clear Linux patchset (tkg)
* BBR3 congestion (CachyOS)
* Glitched TKG base (minimized from tkg)
* 512KB readahead

Config features:
* No CPU vulnerability mitigations
* Madvise hugepages
* Debloated to support only:
    * Lenovo laptops
    * Intel CPUs, GPUs, WiFi
    * Realtek audio codecs and ethernet

